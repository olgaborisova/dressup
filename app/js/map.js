var map;
$(function() {
	var locationsData = {
		msk: {
			center: [55.76962108310323, 37.60757446289063],
			zoom: 11.5
		},
		spb: {
			center: [59.94857233192883, 30.292516608092917],
			zoom: 10.5
		},
		default: {
			center: [58.023516, 33.736090],
			zoom: 5
		}
		
	};
	
	//Config
	var mapConfig = {
		attributionControl: false,
		zoomControl: true,
		center: locationsData.default.center,
		zoom: locationsData.default.zoom,
		minZoom: 4,
		maxZoom: 18,
		zoomSnap: 0.5,

		wheelDebounceTime: 100,
		wheelPxPerZoomLevel: 200
	};

	var mapIconConfig = {
		iconSize: [30, 30],
		iconAnchor: [15, 15],
		popupAnchor: [0, -35],
		className: 'map__marker',
		html: '<svg class="map__marker-svg svg svg_marker"><use xlink:href="#icon-map-marker"></use>'
	};

	var mapIcon = L.divIcon(mapIconConfig);

	var mapPopupConfig = {
		maxWidth: 370,
		minWidth: 370,
		closeButton: true,
		className: 'map__popup',
		autoPanPadding: [20, 20]
	};

	//Map init
	if ($('#map').length) {
		map = new L.map('map', mapConfig);
		L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);
		
		var markers = [];

		$.ajax({
			method: 'GET',
			url: '/map.json',
		})
			.done(function(data) {
				if (data && Array.isArray(data)) {
					addMarkers(data);
				} else {
					console.log("Map: markers loading error");
				}
			})
			.fail(function(error) {
				console.log(error.message || error || "Map: markers loading error");
			});
	}
		
	function addMarkers(markersData) {
		if (!markersData.length) {
			return;
		}
		
		$.each(markersData, function(i, markerData) {
			var marker = new L.marker(markerData.latlng, {icon: mapIcon, riseOnHover: true});
			var popup = new L.popup(mapPopupConfig).setContent(setPopupContent(i, markerData));

			marker.bindPopup(popup);
			markers.push(marker);
		});
		
		var markersLayer = L.markerClusterGroup(
			{
				showCoverageOnHover: false,
				// spiderfyOnMaxZoom: false,
				// disableClusteringAtZoom: 20,
				chunkedLoading: false,
				zoomToBoundsOnClick: false
			}
		);

		markersLayer.on('clusterclick', function(event) {
			event.layer.zoomToBounds({padding: [20, 20]});
		});

		markersLayer.clearLayers();
		markersLayer.addLayers(markers);

		map.addLayer(markersLayer);
		fitMarkersLayer();

		//Change location with dropdown
		$('.js-tabs-map .js-tabs-nav').on('click', function() {
			var locationName = $(this).attr('data-nav-id');
			if (!locationName) {
				return;
			}

			if (locationName === 'all' || !locationsData.hasOwnProperty(locationName)) {
				fitMarkersLayer();
			} else {
				map.setView(locationsData[locationName].center, locationsData[locationName].zoom);
			}
		});
		
		function fitMarkersLayer() {
			map.fitBounds(markersLayer.getBounds(), {maxZoom: 18, padding: [20, 20]});
		}
	}
	
	$('.js-latlng-link').on('click', function(event) {
		var latlngData;
		try {
			latlngData = JSON.parse($(this).attr('data-latlng'));
		}
		catch(error) {
			console.error(error.message || error);
		}

		if (latlngData) {
			map.setView(latlngData, 17);
		}
	});

	function setPopupContent(index, markerData) {
		var popupContentGallery = '';

		if (markerData.gallery && markerData.gallery.length) {
			popupContentGallery += '<div class="map__popup-gallery grid grid_h_between">';
			$.each(markerData.gallery, function(j, imgSrc) {
				popupContentGallery += `<a href="${imgSrc}" class="map__popup-gallery-item" data-fancybox="map-gallery-${index}"><img src="${imgSrc}" alt="shop" class="map__popup-gallery-img js-fit"></a>`;
			});
			popupContentGallery += '</div>';
		}

		return `
<div class="map__popup-wrp">
<div class="map__popup-article article article_s">${markerData.description}</div>
${popupContentGallery}
</div>
	`;
	}
});
