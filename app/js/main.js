var wH = (window.innerHeight || document.documentElement.clientHeight);
var wW = (window.innerWidth || document.documentElement.clientWidth);

var body = document.body,
	html = document.documentElement;

var pageHeight = Math.max(body.scrollHeight, body.offsetHeight,
	html.clientHeight, html.scrollHeight, html.offsetHeight);

var userAgent = navigator.userAgent.toLowerCase(),

	isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
	isTouch = "ontouchstart" in window,

	mobile = false;

if (isMobile || isTouch) {
	mobile = true;
}

var anchorScroll;

$(function() {
	var fitImages = document.querySelectorAll('.js-fit');
	objectFitImages(fitImages, {watchMQ: true});

	var $document = $(document),
		$window = $(window),
		$html = $("html"),
		$body = $("body");

	//Prevent dragging img, links, buttons
	$("img, a, button").on("dragstart", function(event) {
		event.preventDefault();
	});

	//Sticky
	var $stickyEl = $('.js-sticky');
	Stickyfill.add($stickyEl);

	//Scroll
	$window.on('scroll', scrollThrottler);

	var scrollTimeout;
	function scrollThrottler() {
		if (!scrollTimeout) {
			scrollTimeout = setTimeout(function() {
				scrollTimeout = null;
			}, 100);
			//scroll action
		}
	}

	//Card lazy load
	var lazyLoadImg = function($img) {
		$img.attr('src', $img.attr('data-src'));

		$img[0].onload = function() {
			$img.prop('data-src', false);
			$img.addClass('swiper-lazy-loaded').removeClass('swiper-lazy');
			$img.next('.loader').remove()
		};
	};

	var $imgNotSlider = $('.swiper-lazy').filter(function(index) {
		return !$(this).closest('.swiper-container').length;
	});

	$imgNotSlider.each(function() {
		lazyLoadImg($(this));
	});

	//Anchor link
	$('.js-anchor-link').on('click', function(e) {
		e.preventDefault();
		
		anchorScroll($(this).attr('href'), 600);
	});

	anchorScroll = function(elName, time, offset) {
		var targetOffset = elName ? +$(elName).offset().top : 0;

		if (!!offset) {
			targetOffset -= offset;
		}

		$('body, html').animate({scrollTop: targetOffset}, time || 600);
	};


	//Popup
	var openPopup = function(popupSrc) {
		$.fancybox.close();

		$.fancybox.open({
			src: popupSrc,
			type: 'inline',
			opts: {
				animationEffect: "fade",
				animationDuration: 400,
				toolbar: false,
				smallBtn: false,
				autoFocus: true,
				touch: false,
				baseTpl:
					'<div class="fancybox-container" role="dialog" tabindex="-1">' +
					'<div class="fancybox-bg"></div>' +
					'<div class="fancybox-inner">' +
					'<div class="fancybox-stage"></div>' +
					"</div>" +
					"</div>",
			}
		}, {
			baseClass: 'fancybox-popup',
			beforeShow: function(instance, slide) {

			},
			afterClose: function(instance, slide) {

			},
		});
	};

	window.openPopup = openPopup;

	$('.js-popup-link').on('click', function(e) {
		e.preventDefault();
		var popupSrc = $(this).attr('href');
		openPopup(popupSrc);
	});

	$('.js-popup-close').on('click', function(e) {
		e.preventDefault();
		$.fancybox.close();
	});
	
	//Gallery
	var galleryOptions = {
		loop: true,
		toolbar: true,
		buttons: [
			"zoom",
			"share",
			// "slideShow",
			"fullScreen",
			"download",
			"thumbs",
			"close"
		],
		thumbs: {
			autoStart: true,
			hideOnClose: true,
			parentEl: ".fancybox-container",
			axis: "x"
		},
	};
	
	//Product
	$('[data-fancybox^="product-gallery"]').fancybox(galleryOptions);
	
	//Map
	$('[data-fancybox^="map-gallery"]').fancybox(galleryOptions);
	
	//Card
	$('.js-card-preview-toggle').on('click', function() {
		var $galleryItem = $(this).find('.js-card-preview-gallery-item');
		$.fancybox.open(
			$galleryItem,
			galleryOptions,
			3
		);
	});
	
	//Dropdown
	$('.js-dropdown-toggle').on('click', function() {
		$(this).closest('.js-dropdown').toggleClass('open');
	});

	$(document).on('keydown', function(e) {
		if (e.keyCode === 27 || e.which === 27) {
			$('.js-dropdown').removeClass('open');
		}
	});

	var prevDropdown = null;

	$(document).on('click', function(e) {
		var currentDropdown;

		if ($(e.target).hasClass('js-dropdown')) {
			currentDropdown = $(e.target);
		} else if ($(e.target).closest('.js-dropdown').length) {
			currentDropdown = $(e.target).closest('.js-dropdown');
		}
		if (!currentDropdown) {
			$('.js-dropdown').removeClass('open');
		} else {
			if (!currentDropdown.is(prevDropdown) && prevDropdown) {
				prevDropdown.removeClass('open');
			}
			prevDropdown = currentDropdown;
		}
	});

	$('.js-dropdown-link').on('click', function(e) {
		e.preventDefault();
		var dropdown = $(this).closest('.js-dropdown');

		if (!$(this).hasClass('active')) {
			dropdown.find('.js-dropdown-toggle').html($(this).html());
			dropdown.find('.js-dropdown-link.active').removeClass('active');
			$(this).addClass('active');
		}

		dropdown.removeClass('open');
	});
	
	//Tab product color
	$('.js-product-parameter').on('click', function(e) {
		e.preventDefault();
		var parameters = $(this).closest('.js-parameters');
		var gallery = $('.js-product-gallery');
		
			//change parameters
		parameters.find('.js-product-parameter').removeClass('active');
		$(this).addClass('active');

			//change grid
		var currentId = $(this).attr('data-parameter-id');
		var $currentGrid = gallery.find('.js-product-grid[data-grid-id="' + currentId + '"]');
		gallery.find('.js-product-grid').removeClass('active');
		$currentGrid.addClass('active');
		mobileCardSliderInit($currentGrid);
	});

	//Tabs
	$('.js-tabs-nav').on('click', function(e) {
		e.preventDefault();
		var tabs = $(this).closest('.js-tabs');
		
		//nav
		var nav = tabs.find('.js-tabs-nav').filter(function() {
			//get only direct children of current tabs
			return $(this).closest('.js-tabs').is(tabs);
		});
		
		nav.removeClass('active');
		$(this).addClass('active');
		
		
		//content
		var currentId = $(this).attr('data-nav-id');
		var content = tabs.find('.js-tabs-content').filter(function() {
			//get only direct children of current tabs
			return $(this).parent().closest('.js-tabs').is(tabs);
		});
		
		content.removeClass('active');
		var currentContent = content.filter('[data-content-id="' + currentId + '"]');
		if (currentId === 'all') {
			currentContent = content;
		}
		currentContent.addClass('active');
		
		//Contacts scroll to map
		var contacts = $(this).closest('.contacts');
		if (contacts.length) {
			anchorScroll(contacts.find('#map'), 600, 30);
		}
		
		//Set query param
		var queryParam = tabs.attr('data-query-param');
		if (queryParam) {
			var params = new URLSearchParams(location.search);
			params.set(queryParam, currentId);
			var url = `${location.origin}${location.pathname}?${params}${location.hash}`;
			window.history.replaceState({}, document.title, url);
		}
	});

	//Set tab by query param 
	var params = new URLSearchParams(window.location.search);
	if (params.has('tab')) {
		var navId = params.get('tab');
		$('.js-tabs-nav[data-nav-id="'+ navId +'"]').click();
	}

	//Accordion
		$('.js-category-heading').on('click', function(e) {
			e.preventDefault();
			var $parentCategory = $(this).closest('.js-category-block'),
				$otherCategory = $('.js-category-block').not($parentCategory);

			$otherCategory.find('.js-category-list')
				.slideUp('fast').removeClass('open');
			$parentCategory.find('.js-category-list')
				.slideToggle('fast').toggleClass('open');
		});
		
	//More 
	$('.js-more-button').on('click', function(e) {
		e.preventDefault();
		$(this).closest('.js-more-container').find('.js-more-list').toggleClass('open');
		
		//text
		var text = $(this).text();
		var altText = $(this).attr('data-alt-text');
		$(this).attr('data-alt-text', text);
		setTimeout(() => {
			$(this).text(altText);
		}, 100);
	});
	
	//Filter submit position
	var filterForm = $('.js-filter-form');
	var filterSubmit = $('.js-filter-submit');
	$('.js-filter-form input').on('input keyup change', function() {
		var label = $(this).closest('label');
		var labelTop = label.height() / 2;
		var formTop = Math.round(label.position().top);
		filterSubmit.css('top', labelTop + formTop + 'px').addClass('active');
	});

	//Select size
	$('.js-product-size').on('click', function(e) {
		e.preventDefault();
		$(this).closest('.js-product-size-block').find('.js-product-size').removeClass('active');
		$(this).addClass('active');
	});
	
	//Change product button text
	$('.js-product-button-cart').on('click', function(e) {
		e.preventDefault();

		var text = $(this).text();
		var altText = $(this).attr('data-alt-text');
		$(this).text(altText);
		$(this).prop('disabled', true);

		setTimeout(() => {
			$(this).text(text);
			$(this).prop('disabled', false);
		}, 2000);
	});

	$('.js-product-button-fav').on('click', function(e) {
		e.preventDefault();

		var text = $(this).text();
		var altText = $(this).attr('data-alt-text');
		$(this).attr('data-alt-text', text);
		$(this).text(altText);
	});
	
	//Check card state
	var checkCardState = function() {
		if ($('.js-table-row').length === 1) {
			$('.js-cart-state').attr('data-state', 'empty');
			$('.page__container_no-border').removeClass('page__container_no-border');
		}
	};
	
	//Favourite. Remove product to cart
	$('.js-table-add-btn').on('click', function(e) {
		e.preventDefault();

		var row = $(this).closest('.js-table-row');
		row.html('<div class="table__message grid grid_h_center grid_v_center js-table-message">' +
			'<div class="table__message-caption">' + row.attr('data-message') + '</div>' +
			'</div>');
		setTimeout(() => {
			checkCardState();
			row.remove();
		}, 2000);
		
	});
	
	//Cart delete product
	$('.js-table-delete-btn').on('click', function(e) {
		e.preventDefault();
		
		var row = $(this).closest('.js-table-row');
		row.addClass('table__row_invisible');
		setTimeout(() => {
			row.remove();
		}, 300);

		checkCardState();
	});
	
	//Cart add or remove number product 
	$('.js-number-btn').on('click', function(e) {
		e.preventDefault();
		var self = $(this);
		var isPlus = $(this).hasClass('js-number-plus');
		var number = $(this).closest('.js-number');
		number.find('.js-number-btn').prop('disabled', false);
		
		number.attr('data-sum', function(i, attrValue) {
			if (isPlus) {
				attrValue++;
				if (attrValue >= 10) {
					self.prop('disabled', true);
				}
			} else {
				attrValue--;
				if (attrValue <= 1) {
					self.prop('disabled', true);
				}
			}
			
			attrValue = Math.min(10, attrValue);
			attrValue = Math.max(1, attrValue);
			
			return attrValue;
		});
	});
	
	//SLIDERS (main, similar, mobile card)
	//Main slider
	var slider = new Swiper('.js-slider', {
		loop: true,
		autoHeight: false,
		slidesPerView: 1,
		spaceBetween: 0,
		pagination: {
			el: '.js-slider-pagination',
			clickable: true,
			bulletClass: 'slider__pagination-bullet',
			bulletActiveClass: 'slider__pagination-bullet_active'
		},
		speed: 600,
		effect: 'fade',
		fadeEffect: {
			crossFade: true
		},
		keyboard: {
			enabled: true,
			onlyInViewport: true,
		},
	});

	//slider card
	var sliderCard = [];
	var sliderCardSettings = {
		loop: false,
		autoHeight: false,
		slidesPerView: 1,
		spaceBetween: 0,
		speed: 600,
		effect: 'fade',
		fadeEffect: {
			crossFade: true
		},
	};
	
	$('.js-slider-card').each(function(i, item) {
		sliderCard[i] = new Swiper(this, sliderCardSettings);
	});
	
	$('.js-card-preview-link').on('click', function(e) {
		e.preventDefault();
		
		if ($(this).hasClass('active')) return;
		
		var closestPreviewLinks = $(this).closest('.js-card-preview').find('.js-card-preview-link');
		var index = closestPreviewLinks.index($(this));
		var slider = $(this).closest('.js-card').find('.js-slider-card')[0].swiper;
		slider.slideTo(index);

		closestPreviewLinks.removeClass('active');
		$(this).addClass('active');
	});
	
	//Slider similar
	$('.js-slider-similar').each(function(){
		var similarSlider = new Swiper(this, {
			loop: true,
			autoHeight: false,
			slidesPerView: 1,
			spaceBetween: 30,
			keyboard: {
				enabled: true,
				onlyInViewport: true,
			},
			navigation: {
				nextEl: $(this).closest('.js-similar').find('.js-similar-arrow'),
			},

			breakpoints: {
				//Tablet
				768: {
					slidesPerView: 3,
					spaceBetween: 20,
				},
				// Display
				1025: {
					slidesPerView: 3,
					spaceBetween: (($(this).width() - $(this).width()*30.76923076923077/100*3) / 2),
				},
			}
		})
	});

	//Mobile card slider
	let mobileCardSliders = [];

	function mobileCardSliderInit($container) {
		if ($(window).width() > 767) {
			return;
		}

		let id = $container.attr('data-grid-id');

		//Check if slider is already initialized
		if (mobileCardSliders[id]) { 
			mobileCardSliders[id].update();
		} else {
			mobileCardSliders[id] = new Swiper($container, {
				wrapperClass: 'js-product-grid-wrp',
				slideClass: 'js-gallery-item',

				loop: true,
				slidesPerView: 1,
				spaceBetween: 30,
				pagination: {
					el: '.js-product-grid-progress',
					type: 'progressbar',
				},
			});
		}
	}

	mobileCardSliderInit($('.js-product-grid.active'));

	function mobileCardSliderHandleResize() {
		let windowWidth = $(window).width();
		if (windowWidth < 768) {
			if (!mobileCardSliders[$('.js-product-grid.active').attr('data-grid-id')]) {
				mobileCardSliderInit($('.js-product-grid.active'));
			}
		} else {
			mobileCardSliders.forEach(function(slider) {
				slider.destroy(true, true);
			});

			mobileCardSliders = [];
		}
	}

	//Resize
	$(window).on('resize', resizeThrottler);

	let resizeTimeout;
	function resizeThrottler() {
		if (!resizeTimeout) {
			resizeTimeout = setTimeout(function () {
				resizeTimeout = null;
			}, 200);
			mobileCardSliderHandleResize();
		}
	}
	
	//Subscribe form
	var validateEmail = function(email) {
		var regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return regExp.test(email);
	};

	var validateUsername = function(username) {
		var regExp = /^[a-zA-Z0-9_]{1,60}$/;
		return regExp.test(username);
	};
	
	var validatePhone = function(phone) {
		var regExp = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)[\d\- ]{3,10}$/;
		return regExp.test(phone);
	};
	
	var validate = {
		'email': validateEmail,
		'username': validateUsername,
		'phone': validatePhone,
	};

	$('.js-form-input, .js-form-textarea').each(function() {
		checkInput.call(this);
		$(this).on('input keyup', checkInput);
	});
	
	function checkInput() {
		var field = $(this).closest('.js-form-field');
		if ($(this).val().length > 0) {
			field.removeClass('error error-simple').addClass('filled');
		} else {
			field.removeClass('filled');
		}
	}
	
	$('.js-form-required').on('submit', function(e) {
		e.preventDefault();
		var form = $(this);
		var method = form.attr('method');
		var action = form.attr('action');
		var requiredInput = form.find('[data-required]');
		var specialRequiredInput = requiredInput.filter(function() {
			return $(this).attr('data-required').length;
		});
		var errorCount = 0;
	
		requiredInput.each(function(i, input) {
			var field = $(this).closest('.js-form-field');
			
			if ($(this).val().length > 0) {
				field.removeClass('error error-simple');
			} else {
				field.addClass('error-simple');
				errorCount++;
			}
		});

		specialRequiredInput.each(function(i, input) {
			var field = $(this).closest('.js-form-field');
			var type = $(this).attr('data-required');
			var value = $(this).val();
			
			if (value.length === 0 || (typeof validate[type] !== 'function')) {
				return;
			}
			
			if (validate[type](value)) {
				field.removeClass('error error-simple');
			} else {
				setTimeout(function() {
					field.addClass('error');
				},100);
			
				errorCount++;
			}
		});
		
		if (errorCount === 0) {
			$.ajax({
				method: method,
				url: action,
				data: form.serialize()
			})
				.done(function( msg ) {
					var success = form.attr('data-success');
					
					if (form.hasClass('subscribe__form')) {
						//form-subscribe
						form.addClass('result-success');
					} else if (success) {
						if ($(success).length) {
							openPopup(success);
						} else {
							window.location = success;	
						}
					}
					
					form[0].reset();
				})
				.fail(function() {
					form.addClass('result-fail');

					if (form.hasClass('subscribe__form ')) {
						//form-subscribe
						setTimeout(function() {
							form.removeClass('result-fail');
						}, 2000);
					}
				})
				.always(function() {
					form.find('.error').removeClass('error');
					form.find('.error-simple').removeClass('error-simple');
				});
		}
	});	
	
	//Menu
	$('.js-burger-button').on('click', function() {
		$('.js-page').toggleClass('overflow');
		$('.js-menu').slideToggle(150);
		$(this).toggleClass('open');
	});
	
	//Filters
	$('.js-sidebar-toggle').on('click', function() {
		var catalog = $(this).closest('.js-catalog');
		catalog.toggleClass('open');
	});
});
